#include "FirstFitProcessing.h"
#include <string.h>
#include <iostream>
#include <vector>
using namespace std;

FirstFitProcessing::FirstFitProcessing(const char* filename, int bytesForIndex) : VariableLengthFileProcessing(filename, bytesForIndex) {
	
}

int FirstFitProcessing::FindSuitablePos(int strLen, int& posPrevIdx, int& nextIdx, int& lenOfPos) {
	int headIdx = GetHeadIndex();
	int curPos = 0;
	if (headIdx == -1) {//ko co vi tri bi xoa
		return -1;
	}
	else //co vi tri bi xoa
	{
		//di den vi tri duoc danh dau va doc do dai chuoi
		char* buf = new char[bytesForIndex];
		int len;
		int marked = headIdx;
		do
		{
			myfile->get(buf, marked, bytesForIndex);
			buf[bytesForIndex] = '\0';
			//chuyen sang so va so sanh voi strLen
			len = atoi(buf);

			//neu len > strLen thi lay idx ke tiep luu lai
			if (len >= strLen) {
				lenOfPos = len;
				posPrevIdx = curPos;
				myfile->get(buf, marked + bytesForIndex, bytesForIndex);
				buf[bytesForIndex] = '\0';
				nextIdx = atoi(buf);
				return marked + bytesForIndex;
			}
			else {
				curPos = marked + bytesForIndex;
				myfile->get(buf, marked + bytesForIndex, bytesForIndex);
				buf[bytesForIndex] = '\0';
				marked = atoi(buf);
			}
		} while (marked != -1);
	}
	return -1;
}