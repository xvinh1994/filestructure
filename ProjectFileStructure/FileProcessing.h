#ifndef FILEPROCESSING_H
#define FILEPROCESSING_H
#include "MyFile.h"

class FileProcessing {
protected:
	MyFile* myfile;
	const char* filename;
public:
	FileProcessing(const char* filename);

	virtual void Add(const char* player) = 0;
	virtual void Remove(const char* str) = 0;
};
#endif