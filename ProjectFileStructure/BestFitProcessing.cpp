#include "BestFitProcessing.h"
#include <string.h>
#include <iostream>
#include <vector>
using namespace std;

BestFitProcessing::BestFitProcessing(const char* filename, int bytesForIndex) : VariableLengthFileProcessing(filename, bytesForIndex) {

}

int BestFitProcessing::FindSuitablePos(int strLen, int& posPrevIdx, int& nextIdx, int& lenOfPos) {
	int headIdx = GetHeadIndex();
	int curPos = 0;
	if (headIdx == -1) {//ko co vi tri bi xoa
		return -1;
	}
	else //co vi tri bi xoa
	{
		//di den vi tri duoc danh dau va doc do dai chuoi
		char* buf = new char[bytesForIndex];
		int len;
		int minLen = INT32_MAX;
		int result = -1;
		int marked = headIdx;
		do
		{
			myfile->get(buf, marked, bytesForIndex);
			buf[bytesForIndex] = '\0';
			//chuyen sang so va so sanh voi strLen
			len = atoi(buf);

			//neu len > strLen thi lay idx ke tiep luu lai
			if (len == strLen) {
				lenOfPos = len;
				posPrevIdx = curPos;
				myfile->get(buf, marked + bytesForIndex, bytesForIndex);
				buf[bytesForIndex] = '\0';
				nextIdx = atoi(buf);
				return marked + bytesForIndex;
			}
			else if (len > strLen) {
				myfile->get(buf, marked + bytesForIndex, bytesForIndex);
				buf[bytesForIndex] = '\0';
				if (minLen > len) {
					minLen = len;
					lenOfPos = len;
					posPrevIdx = curPos;
					nextIdx = atoi(buf);
					result = marked + bytesForIndex;
				}
				curPos = marked + bytesForIndex;
				marked = atoi(buf);
			}
			else {
				curPos = marked + bytesForIndex;
				myfile->get(buf, marked + bytesForIndex, bytesForIndex);
				buf[bytesForIndex] = '\0';
				marked = atoi(buf);
			}
		} while (marked != -1);
		return result;
	}
}