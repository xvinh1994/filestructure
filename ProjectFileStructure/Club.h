#ifndef CLUB_H
#define CLUB_H
#include "BestFitProcessing.h"
#include "FirstFitProcessing.h"
#include <string>

class Club {
private:
	VariableLengthFileProcessing* playerWriter;
	const char* clubname;
	const char* season;
public:
	Club(const char* clubname, const char* season, int method);
	void AddPlayer(const char* player);
	void RemovePlayer(const char* player);
	void Defragement();
};
#endif