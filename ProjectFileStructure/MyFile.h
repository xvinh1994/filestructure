//
// Created by Thuan Le Dinh on 10/6/15.
//

#ifndef MYFILE_H
#define MYFILE_H

#include <stdio.h>
#include <stdlib.h>

class MyFile{
	
private:
	FILE* pFile;
	int read_time;
	int write_time;
	int seek_time;
	void initialize(const char* filename, int read_time, int write_time, int seek_time);

public:
	static const int DEFAULT_READ_TIME = 1;
	static const int DEFAULT_WRITE_TIME = 3;
	static const int DEFAULT_SEEK_TIME = 0;

	MyFile(const char* filename);
	MyFile(const char* filename, int read_time, int write_time, int seek_time);
	~MyFile();
	
	void append(const char *value);
	void put(int pos, const char *value);
	void get(char* buff, int pos, int count);
	void truncate(int newLength);
	void clone(const char* newName);
};


#endif //MYFILE_H
