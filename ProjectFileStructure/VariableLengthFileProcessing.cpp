#include "VariableLengthFileProcessing.h"
#include <string.h>
#include <iostream>
#include <vector>
using namespace std;

VariableLengthFileProcessing::VariableLengthFileProcessing(const char* filename,  int bytesForIndex) : FileProcessing(filename) {
	if (bytesForIndex < 2)
		this->bytesForIndex = 2;
	else
		this->bytesForIndex = bytesForIndex;

	this->fLength = this->bytesForIndex;//initial length

	myfile->append(NormalizeNumber(-1));
}

void VariableLengthFileProcessing::Add(const char* player) {
	int strLen = strlen(player);
	int posPrevIdx = -1, nextIdx = -1, lenOfPos = -1;
	int posToStartWriting = FindSuitablePos(strLen, posPrevIdx, nextIdx, lenOfPos);

	if (posToStartWriting == -1)
	{
		fLength += strLen + bytesForIndex + 1;//<idx><data>|
		myfile->append(NormalizeNumber(strLen));//3: len(club) = 3
		myfile->append(NormalizeString(player, strLen));
		myfile->append("|");
	}
	else {
		myfile->put(posToStartWriting, NormalizeString(player, lenOfPos));//viet vao vi tri dc chon
		myfile->put(posPrevIdx, NormalizeNumber(nextIdx));//viet vi tri idx bi xoa ke tiep
	}
}

char* VariableLengthFileProcessing::NormalizeNumber(int number) {
	if (number < -1 || number == 0) return NormalizeNumber(-1);

	char* returnNum = new char[bytesForIndex];
	memset(returnNum, '0', bytesForIndex);
	returnNum[bytesForIndex] = '\0';

	if (number == -1) {
		returnNum[0] = '-';
		number = 1;
	}

	int pos = bytesForIndex - 1;
	for (int i = number; i > 0; i /= 10)
	{
		int remainder = i % 10;
		char c = remainder + 48;
		returnNum[pos] = c;
		pos--;
	}

	return returnNum;
}

char* VariableLengthFileProcessing::NormalizeString(const char* str, int length) {
	char* lengthStr = new char[length];
	memset(lengthStr, '%', length);
	lengthStr[length] = '\0';

	int strLen = strlen(str);
	int len = strLen < length ? strLen : length;
	memcpy(lengthStr, str, len);

	return lengthStr;
}

int VariableLengthFileProcessing::GetHeadIndex() {
	int pos = -1;
	char* buf = new char[bytesForIndex];

	myfile->get(buf, 0, bytesForIndex);

	if (buf[0] != '-')//ko phai -1
	{
		pos = atoi(buf);
	}

	return pos;
}

void VariableLengthFileProcessing::Remove(const char* str) {
	vector<int> posToRemove = ListPosToRemove(str);
	RewriteFile(posToRemove);
}

vector<int> VariableLengthFileProcessing::ListPosToRemove(const char* str) {
	vector<int> result;
	char* buf = new char[bytesForIndex];
	int curPos = bytesForIndex;//bo so byte cho -1
	int strLen = strlen(str);

	while (curPos < fLength)
	{
		myfile->get(buf, curPos, bytesForIndex);
		curPos += bytesForIndex;
		buf[bytesForIndex] = '\0';
		int len = atoi(buf);

		if (len >= strLen) {
			int tmpPos = curPos;
			char* tmpBuf = new char[1];
			bool isOk = true;
			
			for (int i = 0; i < len; i++)
			{
				myfile->get(tmpBuf, tmpPos++, 1);//lay 1 ky tu ke tiep
				tmpBuf[1] = '\0';
				if (i >= strLen) break;
				else if (tmpBuf[0] != str[i]){
					isOk = false;
					break;
				}
			}

			if (isOk) result.push_back(curPos - bytesForIndex);
			
		}
		curPos += len + 1;//len data + 1 cho |
	};


	return result;
}

void VariableLengthFileProcessing::RewriteFile(vector<int>& posToRemove) {
	int vectorSize = posToRemove.size();

	//stack style
	for (int i = 0; i < vectorSize; i++)
	{
		int headIndex;
		if (i == 0) headIndex = GetHeadIndex();
		else headIndex = posToRemove[i - 1];
		if (i == vectorSize - 1) {
			char* index = NormalizeNumber(posToRemove[i]);
			myfile->put(0, index);//ghi index vao dau file
		}
		myfile->put(posToRemove[i] + bytesForIndex, NormalizeNumber(headIndex));//ghi headIndex cu vao vi tri index
	}
}

void VariableLengthFileProcessing::Defragment(const char* destFile) {
	MyFile* dest = new MyFile(destFile);
	int newLength = bytesForIndex;
	int curPosRead = bytesForIndex;
	map<int, int> listPosRemoved = GetPosRemoved();

	dest->put(0, NormalizeNumber(-1));
	while (curPosRead < fLength) {
		if (listPosRemoved.find(curPosRead) != listPosRemoved.end()) {//co curPosRead trong map
			map<int, int>::iterator it = listPosRemoved.find(curPosRead);
			curPosRead += it->second + bytesForIndex + 1;
		}
		else {
			char* lenBuf = new char[bytesForIndex];
			myfile->get(lenBuf, curPosRead, bytesForIndex);//lay do dai chuoi dc luu
			curPosRead += bytesForIndex;
			int len = atoi(lenBuf);

			char* buf = new char[len];
			myfile->get(buf, curPosRead, len);//doc toan bo chuoi voi do dai tren (bao gom ca %)
			buf[len] = '\0';
			curPosRead += len + 1;

			char* newStr = EliminateChar(buf);//loai bo dau %
			dest->append(NormalizeNumber(strlen(newStr)));
			dest->append(newStr);//ghi chuoi moi da loai bo % vao file
			dest->append("|");
		}
	};
}

map<int, int> VariableLengthFileProcessing::GetPosRemoved() {
	map<int, int> result;
	int idx = GetHeadIndex();

	int len;
	char* buf = new char[bytesForIndex];

	while (idx != -1) {
		myfile->get(buf, idx, bytesForIndex);
		len = atoi(buf);
		result[idx] = len;
		myfile->get(buf, idx + bytesForIndex, bytesForIndex);
		idx = atoi(buf);
	};
	return result;
}

char* VariableLengthFileProcessing::EliminateChar(const char* blockStr) {
	int len = strlen(blockStr);
	string newStr= "";

	for (int i = 0; i < len; i++)
	{
		if (blockStr[i] != '%') {
			newStr += blockStr[i];
		}
	}

	char* result = new char[newStr.length() + 1];
	strcpy(result, newStr.c_str());

	return result;
}