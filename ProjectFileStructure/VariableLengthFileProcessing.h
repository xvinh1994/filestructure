#ifndef VARIABLELENGTHFILEPROCESSING_H
#define VARIABLELENGTHFILEPROCESSING_H

#include "FileProcessing.h"
#include <vector>
#include <map>
using namespace std;

class VariableLengthFileProcessing : public FileProcessing {
protected:
	int bytesForIndex;
	int fLength;

	virtual int FindSuitablePos(int strLen, int& posPrevIdx, int& nextIdx, int& lenOfPos) = 0;
	int GetHeadIndex();

	char* NormalizeString(const char* str, int length);
	char* NormalizeNumber(int number);

	vector<int> ListPosToRemove(const char* str);

	void RewriteFile(vector<int>& posToRemove);
public:
	VariableLengthFileProcessing(const char* filename, int bytesForIndex);

	void Defragment(const char* destFile);
	void Remove(const char* str) override;
	void Add(const char* player) override;
private:
	map<int, int> GetPosRemoved();
	char* EliminateChar(const char* blockStr);
};
#endif