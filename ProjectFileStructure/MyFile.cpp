//
// Created by Thuan Le Dinh on 10/6/15.
//

#include <cstdio>
#include <iostream>
#include "MyFile.h"

#ifdef __APPLE__
#include <sys/uio.h>
#else
#include "io.h"
#endif

using namespace std;

MyFile::MyFile(const char *filename) {
    initialize(filename, DEFAULT_READ_TIME, DEFAULT_WRITE_TIME, DEFAULT_SEEK_TIME);
}

MyFile::MyFile(const char *filename, int read_time, int write_time, int seek_time) {
    initialize(filename, DEFAULT_READ_TIME, DEFAULT_WRITE_TIME, DEFAULT_SEEK_TIME);
}

void MyFile::initialize(const char *filename, int read_time, int write_time, int seek_time) {
    this->read_time = read_time;
    this->write_time = write_time;
    this->seek_time = seek_time;
    
    pFile = fopen(filename, "w+b");
}

MyFile::~MyFile() {
    if (pFile) {
        fclose(pFile);
    }
}


void MyFile::append(const char *value) {
    fseek(pFile, 0, SEEK_END);
    fputs(value, pFile);
}

void MyFile::get(char* buf, int pos, int count) {
    fseek(pFile, pos, SEEK_SET);
    fread(buf, 1, count, pFile);
}

void MyFile::put(int pos, const char *value) {
    fseek(pFile, pos, SEEK_SET);
    fputs(value, pFile);
}



void MyFile::truncate(int newLength) {
    //int fd = fileno(pFile);
    //ftruncate(fd, newLength);
	int fd = _fileno(pFile);
	_chsize(fd, newLength);
}

void MyFile::clone(const char* newName) {

}