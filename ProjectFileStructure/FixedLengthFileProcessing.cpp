#include "FixedLengthFileProcessing.h"
#include <string.h>
#include <iostream>
#include <vector>
using namespace std;

FixedLengthFileProcessing::FixedLengthFileProcessing(const char* filename, int oLength, int bytesForIndex) : FileProcessing(filename) {
	this->oLength = oLength;

	if (bytesForIndex > oLength)
		this->bytesForIndex = oLength;
	else if (bytesForIndex < 2)
		this->bytesForIndex = 2;
	else
		this->bytesForIndex = bytesForIndex;

	this->fLength = this->bytesForIndex;//initial length

	myfile->append(NormalizeNumber(-1));
}

void FixedLengthFileProcessing::Add(const char* str) {
	char* fixedLengthStr = NormalizeString(str);
	int nextIndex = -1;
	int posToStartWriting = GetHeadIndex(nextIndex);

	if (posToStartWriting == -1)
	{
		fLength += strlen(fixedLengthStr);
		myfile->append(fixedLengthStr);
	}
	else {
		char* nextIndexStr = NormalizeNumber(nextIndex);
		myfile->put(posToStartWriting, fixedLengthStr);
		myfile->put(0, nextIndexStr);
	}
}

void FixedLengthFileProcessing::Remove(const char* str) {
	char* normalizedStr = NormalizeString(str);
	vector<int> posToRemove;

	ListPosToRemove(normalizedStr, posToRemove);
	RewriteFile(posToRemove);
}

char* FixedLengthFileProcessing::NormalizeString(const char* str) {
	char* fixedLengthStr = new char[oLength];
	memset(fixedLengthStr, '.', oLength);//tao oLength dau cham va gan vao fixedLengthStr
	fixedLengthStr[oLength] = '\0';

	int strLen = strlen(str);
	int len = strLen < oLength ? strLen : oLength;
	memcpy(fixedLengthStr, str, len);

	return fixedLengthStr;
}

char* FixedLengthFileProcessing::NormalizeNumber(int number) {
	if (number < -1 || number == 0) return NormalizeNumber(-1);

	char* returnNum = new char[bytesForIndex];
	memset(returnNum, '0', bytesForIndex);
	returnNum[bytesForIndex] = '\0';

	if (number == -1) {
		returnNum[0] = '-';
		number = 1;
	}

	int pos = bytesForIndex - 1;
	for (int i = number; i > 0; i /= 10)
	{
		int remainder = i % 10;
		char c = remainder + 48;
		returnNum[pos] = c;
		pos--;
	}

	return returnNum;
}

bool FixedLengthFileProcessing::Compare(const char* fromFile, const char* fromInput) {
	int len = strlen(fromInput);
	for (int i = 0; i < len; i++)
	{
		char c1 = fromFile[i];
		char c2 = fromInput[i];
		if (c1 != c2) return false;
	}

	return true;
}

void FixedLengthFileProcessing::ListPosToRemove(const char* normalizedStr, vector<int>& posToRemove) {
	char* buf = new char[oLength];
	//0                  10                19
	//a b c . . . . . . . a b c . . . . . . . => fLength = 20
	for (int i = bytesForIndex; i <= fLength - oLength; i += oLength)
	{
		myfile->get(buf, i, 1);//lay 1 ky tu
		if (buf[0] == normalizedStr[0])
		{
			myfile->get(buf, i, oLength);// lay het do dai chuoi de tranh truong hop chuoi dac biet
			buf[oLength] = '\0';
			if (Compare(buf, normalizedStr))
			{
				posToRemove.push_back(i);
			}
		}
	}
}

void FixedLengthFileProcessing::RewriteFile(vector<int>& posToRemove) {
	int vectorSize = posToRemove.size();

	//stack style
	for (int i = 0; i < vectorSize; i++)
	{
		int headIndex = GetHeadIndex();
		char* index = NormalizeNumber(posToRemove[i]);
		myfile->put(0, index);//ghi index vao dau file
		myfile->put(posToRemove[i], NormalizeNumber(headIndex));//ghi headIndex cu vao vi tri index
	}
}

int FixedLengthFileProcessing::GetHeadIndex(int& nextIndex) {
	int pos = -1;
	char* buf = new char[bytesForIndex];

	myfile->get(buf, 0, bytesForIndex);

	if (buf[0] != '-')//ko phai -1
	{
		pos = atoi(buf);

		myfile->get(buf, pos, bytesForIndex);//toi index ke tiep
		nextIndex = atoi(buf);
	}

	return pos;
}

int FixedLengthFileProcessing::GetHeadIndex() {
	int pos = -1;
	char* buf = new char[bytesForIndex];

	myfile->get(buf, 0, bytesForIndex);

	if (buf[0] != '-')//ko phai -1
	{
		pos = atoi(buf);
	}

	return pos;
}

void FixedLengthFileProcessing::Defragment() {
	vector<int> listPosRemoved = GetPosRemoved();
	int newLength = bytesForIndex;
	int curPosRead = bytesForIndex;
	int curPosRewrite = bytesForIndex;

	myfile->put(0, NormalizeNumber(-1));
	while (curPosRead < fLength) {
		bool found = false;
		for (int i = 0; i < listPosRemoved.size(); i++)
		{
			if (listPosRemoved[i] == curPosRead) {
				found = true;
				listPosRemoved.erase(listPosRemoved.begin() + i);
				break;
			}
		}

		if (found) {
			curPosRead += oLength;
		}
		else {
			char* buf = new char[oLength];
			myfile->get(buf, curPosRead, oLength);
			buf[oLength] = '\0';
			myfile->put(curPosRewrite, buf);
			curPosRewrite += oLength;
			curPosRead += oLength;
			newLength += oLength;
		}
	}
	myfile->truncate(newLength);
}

vector<int> FixedLengthFileProcessing::GetPosRemoved() {
	vector<int> result;
	int idx = GetHeadIndex();

	char* buf = new char[bytesForIndex];

	while (idx != -1) {
		result.push_back(idx);
		myfile->get(buf, idx, bytesForIndex);
		idx = atoi(buf);
	};
	return result;
}