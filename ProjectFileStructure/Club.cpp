#include "Club.h"

Club::Club(const char* clubname, const char* season, int method) {
	this->clubname = clubname;
	this->season = season;
	string* filename = new string(string(clubname, strlen(clubname)) + string(".txt", 4));
	if (method == 0)
		playerWriter = new BestFitProcessing(filename->data(), 4);
	else playerWriter = new FirstFitProcessing(filename->data(), 4);
}

void Club::AddPlayer(const char* player) {
	playerWriter->Add(player);
}

void Club::RemovePlayer(const char* player) {
	playerWriter->Remove(player);
}

void Club::Defragement() {
	string* filename = new string(string(clubname, strlen(clubname)) + "." + string(season, strlen(season)));
	playerWriter->Defragment(filename->data());
}