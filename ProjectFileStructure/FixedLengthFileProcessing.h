#ifndef FIXEDLENGTHFILEPROCESSING_H
#define FIXEDLENGTHFILEPROCESSING_H
#include "FileProcessing.h"
#include <vector>
#include <map>
using namespace std;

class FixedLengthFileProcessing : public FileProcessing {
protected:
	int oLength;
	int bytesForIndex;
	int fLength;
public:
	FixedLengthFileProcessing(const char* filename, int oLength, int bytesForIndex);
	void Add(const char* str) override;
	void Remove(const char* str) override;
	void Defragment();
private:
	int GetHeadIndex(int& nextIndex);
	int GetHeadIndex();

	char* NormalizeString(const char* str);
	char* NormalizeNumber(int number);

	bool Compare(const char* x, const char* y);

	void ListPosToRemove(const char* normalizedStr, vector<int>& posToRemove);
	void RewriteFile(vector<int>& posToRemove);

	vector<int> GetPosRemoved();
};
#endif