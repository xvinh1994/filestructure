#ifndef BESTFITPROCESSING_H
#define BESTFITPROCESSING_H

#include "VariableLengthFileProcessing.h"
#include <vector>
using namespace std;

class BestFitProcessing : public VariableLengthFileProcessing {
public:
	BestFitProcessing(const char* filename, int bytesForIndex);
protected:
	int FindSuitablePos(int strLen, int& posPrevIdx, int& nextIdx, int& lenOfPos) override;
};
#endif