#include "Season.h"

Season::Season(const char* season, int method) {
	this->season = season;
	this->method = method;
}

void Season::CreateClub(const char* clubname) {
	if (clubs->find(clubname) == clubs->end() || clubs->size() == 0)
	{
		Club* c = new Club(clubname, season, method);
		pair<string, Club*> club(clubname, c);
		clubs->insert(club);
	}
}

void Season::ExchangePlayer(int methodExchange, const char* clubname, const char* playername) {
	Club* club;
	if (clubs->find(clubname) == clubs->end())
		CreateClub(clubname);
	club = clubs->at(clubname);
	if (methodExchange == 1) {
		club->AddPlayer(playername);
	}
	else {
		club->RemovePlayer(playername);
	}
}

void Season::EndSeason() {
	for (map<string, Club*>::iterator it = clubs->begin(); it != clubs->end(); it++)
	{
		string clubname = it->first;
		it->second->Defragement();
	}
}

void Season::StartSeason() {
	clubs = new map<string, Club*>();
}

void Season::SetSeason(const char* season) {
	this->season = season;
}