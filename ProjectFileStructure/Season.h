#ifndef SEASON_H
#define SEASON_H
#include <map>
#include <iostream>
#include "Club.h"
using namespace std;

class Season {
private:
	const char* season;
	int method;
	map<string, Club*>* clubs;
public:
	Season(const char* season, int method);
	void StartSeason();
	void CreateClub(const char* clubname);
	void ExchangePlayer(int methodExchange, const char* clubname, const char* playername);
	void EndSeason();
	void SetSeason(const char* season);
};
#endif