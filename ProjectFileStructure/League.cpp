#include "League.h"
#include <string>
#include <iostream>
#include <sstream>
using namespace std;

League::League(const char* inputFile) {
	myStream.open(inputFile);
	clubWriter = new FixedLengthFileProcessing("club.txt", 3, 2);
}

void League::StartLeague() {
	if (myStream.is_open()) {
		string firstNum;
		myStream >> firstNum;
		int numSeason = atoi(firstNum.c_str());
		string method;
		myStream >> method;
		int variableMethod = atoi(method.c_str());

		for (int i = 1; i <= numSeason; i++)
		{
			string* seasonid = new string("s" + to_string(i));
			Season* s;
			if(i == 1) s = new Season(seasonid->c_str(), variableMethod);
			else {
				s = new Season(*seasons["s1"]);
				s->SetSeason(seasonid->data());
			}
			seasons[*seasonid] = s;
		}
	}
	else throw new exception("File not found");
}

void League::EndLeauge() {
	myStream.close();
	clubWriter->Defragment();
}

void League::OnGoingLeague() {
	for (int i = 1; i <= seasons.size(); i++)
	{
		string id = "s" + to_string(i);
		Season* s = seasons.at(id);
		s->StartSeason();
		int clubChanges, playerChanges;
		GetChanges(clubChanges, playerChanges);

		ApplyClubChanges(clubChanges);
		ApplyPlayerChanges(*s, playerChanges);

		s->EndSeason();
	}
}

void League::GetChanges(int& clubChanges, int& playerChanges) {
	string lineclubChanges, linePlayerChanges;
	myStream >> lineclubChanges >> linePlayerChanges;
	clubChanges = stoi(lineclubChanges);
	playerChanges = stoi(linePlayerChanges);
}

void League::ApplyClubChanges(int clubChanges) {
	for (int i = 0; i < clubChanges; i++)
	{
		string method, clubname;
		myStream >> method >> clubname;

		if (method == "1") 
			clubWriter->Add(clubname.c_str());
		else 
			clubWriter->Remove(clubname.c_str());
	}
}

void League::ApplyPlayerChanges(Season& s, int playerChanges) {
	for (int i = 0; i < playerChanges || myStream.eof(); i++)
	{
		string* method = new string(), *clubname = new string(), playername;
		myStream >> *method >> *clubname;
		getline(myStream, playername);
		playername = StringTrim(playername);
		s.CreateClub(clubname->data());
		s.ExchangePlayer(stoi(*method), clubname->data(), playername.c_str());
	}
}

string League::StringTrim(string& str) {
	size_t first = str.find_first_not_of(' ');
	size_t last = str.find_last_not_of(' ');
	return str.substr(first, (last - first + 1));
}