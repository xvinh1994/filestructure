#ifndef FIRSTFITPROCESSING_H
#define FIRSTFITPROCESSING_H

#include "VariableLengthFileProcessing.h"
#include <vector>
using namespace std;

class FirstFitProcessing : public VariableLengthFileProcessing {
public:
	FirstFitProcessing(const char* filename, int bytesForIndex);
protected:
	int FindSuitablePos(int strLen, int& posPrevIdx, int& nextIdx, int& lenOfPos) override;
};
#endif