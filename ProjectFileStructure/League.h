#include <iostream>
#include <fstream>
#include <map>
#include "FixedLengthFileProcessing.h"
#include "Season.h"

class League {
private:
	ifstream myStream;
	FixedLengthFileProcessing* clubWriter;
	map<string, Season*> seasons;

	void GetChanges(int& clubChanges, int& playerChanges);
	void ApplyClubChanges(int clubChanges);
	void ApplyPlayerChanges(Season& s, int playerChanges);

	string StringTrim(string& str);
public:
	League(const char* inputFile);
	void StartLeague();
	void OnGoingLeague();
	void EndLeauge();
};